package com.devcamp.j01_javabasic.s50;

public class Voucher {
    public String voucherCode = "V000000";

    public String getVoucherCode() {
        return voucherCode;
    }
    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }
    public void showVoucher() {
        System.out.println("Voucher code is " + this.voucherCode);
    }
    public void showVoucher(String voucherCode) {
        System.out.println("This is voucher code: " + this.voucherCode);
        System.out.println("This is voucher code too: " + voucherCode);
    }
    public static void main(String[] args) throws Exception {
        Voucher voucher = new Voucher();
        // 0. chạy class xong comment lại dòng này
        voucher.showVoucher();

        // 1. bỏ comment dòng dưới rồi chạy lại class
        //voucher.showVoucher("CODE8888");

        /* 2. Comment lại dòng code trên
         * bỏ comment 2 dòng dưới rồi chạy lại class
         */
        //voucher.voucherCode = "CODE2222";
        //voucher.showVoucher("0008888");

        /* 3. Comment lại 2 dòng code trên
         * bỏ comment các dòng dưới rồi chạy lại class
         */
        // String code = "VOUCHER";
        // voucher.setVoucherCode(code);
        // System.out.println(voucher.getVoucherCode());
        // voucher.showVoucher(voucher.getVoucherCode());
        // voucher.showVoucher();
    }
}
